#include <iostream>
#include <string>
#include <math.h>

#include "ros/ros.h"
#include "eigen3/Eigen/Eigen"
#include <binders.h>

//#define STATE_SIZE 1
//#define NOISE_VECTOR 1
#define MAHA_THRESHOLD 6.6


class ekf_slam
{
public:
    ekf_slam();
    ~ekf_slam();

public:
    void setProcessModel(Eigen::VectorXd x, Eigen::MatrixXd p, Eigen::MatrixXd f_x, Eigen::MatrixXd f_n, Eigen::MatrixXd t_noise);
    void setMeasurementModel(Eigen::VectorXd z_est, Eigen::VectorXd v, Eigen::MatrixXd h_x,
                             Eigen::MatrixXd h_z, Eigen::MatrixXd s, Eigen::MatrixXd r, Eigen::MatrixXd k);
    double prediction();
    double update();
    void clearPlanes();
    void setObservationData(int id, double data);
    void setStateandMeasurementSize(int state, int measurement);

    struct obs_vector {
        int obs_id;
        double data;
        double maha_dist;
    };

protected:

    int state_size, measurement_size;
    std::vector<obs_vector> current_planes;
    std::vector<obs_vector> map, map_filtered;
    double z_map_est;
    Eigen::VectorXd x_kk;
    Eigen::MatrixXd p_kk;
    Eigen::MatrixXd F_x, F_n,T;
    Eigen::VectorXd z_est_map, V;
    Eigen::MatrixXd H_x, H_z, S, R, K, I;
 };
