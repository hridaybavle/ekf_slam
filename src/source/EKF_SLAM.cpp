#include "EKF_SLAM.h"

typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXdd;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> MatrixXdd;

ekf_slam::ekf_slam()
{
    std::cout << "EKF_SLAM constructor " << std::endl;
}

ekf_slam::~ekf_slam()
{
    std::cout << "EKF_SLAM destructor " << std::endl;
}

inline bool comparator(const ekf_slam::obs_vector& lhs, const ekf_slam::obs_vector& rhs)
{
    return lhs.maha_dist < rhs.maha_dist;
}

void ekf_slam::setStateandMeasurementSize(int state, int measurement)
{
    state_size = state;
    measurement_size = measurement;

}

void ekf_slam::setProcessModel(Eigen::VectorXd x,Eigen::MatrixXd p, Eigen::MatrixXd f_x, Eigen::MatrixXd f_n, Eigen::MatrixXd t_noise)
{

    x_kk = x;
    p_kk = p;
    F_x  = f_x;
    F_n  = f_n;
    T    = t_noise;

    //std::cout << "initial map " << map[0].data << std::endl;
    //std::cout << "x_kk " << x_kk << " " << "p_kk " << p_kk <<  " " << "F_x" << F_x << " " << "F_n "  << F_n << " " << "T" << T << std::endl;

    return;

}

void ekf_slam::setMeasurementModel(Eigen::VectorXd z_est, Eigen::VectorXd v, Eigen::MatrixXd h_x,
                                   Eigen::MatrixXd h_z, Eigen::MatrixXd s, Eigen::MatrixXd r, Eigen::MatrixXd k)
{
    z_est_map = z_est;
    V = v;
    H_x = h_x;
    H_z = h_z;
    S = s;
    R = r;
    K = k;
    //std:: cout << "z_est_map " << z_est_map << " " << "V " << V << " " << "H_x " << H_x << " " << "H_z" << H_z << " " << "S" << S << " " << "R" << R << std::endl;

    map.clear();
    obs_vector initial_obs;
    initial_obs.obs_id = 0;
    initial_obs.data = 0.0;
    map.push_back(initial_obs);

}

void ekf_slam::clearPlanes()
{
    current_planes.clear();
}

void ekf_slam::setObservationData(int id, double data)
{

    obs_vector obs;
    obs.obs_id = id;
    obs.data   = data;

    current_planes.push_back(obs);

    //    for(int i =0; i < current_planes.size(); ++i)
    //        std::cout << "current_planes " << current_planes[i].data << std::endl;
}

double ekf_slam::prediction()
{
    //TODO:Make the model nonlinear
    x_kk = F_x*x_kk;
    p_kk = F_x*p_kk*F_x.transpose().eval() + F_n*T*F_n.transpose().eval();

    //std::cout << "x_kk " << x_kk(0) << std::endl;
    return x_kk(0);
}

double ekf_slam::update()
{
    int obs_size = 0;
    std::vector<double> H_z_vec, not_matched_planes;
    MatrixXdd H_z_resized, R_resized;
    VectorXdd V_resized, H_x_resized;

    not_matched_planes.clear();
    for(int i =0; i < current_planes.size(); ++i)
    {
        map_filtered.clear();
        z_map_est = 0;
        for (int j=0; j< map.size(); ++j)
        {
            double dis_maha =0;
            z_map_est = x_kk(0) - current_planes[i].data;
            //std::cout << "z_map_est " << z_map_est << std::endl;
            V(0) = map[j].data - z_map_est;
            //std::cout << "v " << V << std::endl;
            S = H_x * p_kk * H_x.transpose().eval() + H_z * R * H_z.transpose().eval();
            //std::cout << "S " << S << std::endl;
            dis_maha = (V.transpose().eval())*S.inverse()*V;

            //            std::cout << "plane from camera " << current_planes[i].data << std::endl;
            //            std::cout << "plane estimated "  << z_map_est << std::endl;
            //std::cout << "dis_maha when using plane " << i << " data " << current_planes[i].data << " with map " << j << " data " << map[j].data << " is "
            //          << dis_maha << std::endl;

            if (dis_maha < MAHA_THRESHOLD)
            {
                map[j].maha_dist = dis_maha;
                map_filtered.push_back(map[j]);
            }
        }

        if (!map_filtered.empty())
        {
            //            for (int k=0; k<map_filtered.size(); k++)
            //                std::cout << "map filtered elements " << map_filtered[k] << std::endl;

            auto min_element = *std::min_element(map_filtered.begin(), map_filtered.end(), comparator);
            //            std::cout << "min element " << min_element.data << std::endl;
            //            std::cout << "min element id " << min_element.obs_id << std::endl;
            //std::cout << "matched current plane element " << i  << " data " << current_planes[i].data << " with map element " <<  min_element.obs_id << " data " << min_element.data << std::endl;

            z_map_est = x_kk(0) - current_planes[i].data;

//            for (int h=0; h< map.size(); h++)
//            {
//                std::cout << "map data " << map[h].data << std::endl;
//            }

            //std::cout << "map element matched data " <<  map[min_element.obs_id].data << std::endl;
            V(0) = min_element.data - z_map_est;

            V_resized.conservativeResize(obs_size+1);
            V_resized.row(obs_size).setZero();
            V_resized(obs_size,0) = V(0);
            //std::cout << "V_resized " << V_resized << std::endl;

            H_x(0,0) =  1;
            H_z(0,0) = -1;

            H_x_resized.conservativeResize(obs_size+1);
            H_x_resized.row(obs_size).setZero();
            H_x_resized(obs_size,0) = H_x(0,0);
            //std::cout << "H_x_resized " << H_x_resized << std::endl;


            H_z_vec.push_back(H_z(0,0));
            //std::cout << "H_z_vec " << H_z_vec[obs_size] << std::endl;

            obs_size = obs_size + 1;
            //std::cout << "obs_size " << obs_size << std::endl;

        }
        else
        {   //TODO: take this condition to the end of the function
            not_matched_planes.push_back(current_planes[i].data);

        }

    }

    //    std::cout << "obs_size " << obs_size << std::endl;
    H_z_resized.conservativeResize(obs_size,obs_size);
    H_z_resized.setZero();
    for (int i=0; i < obs_size; ++i)
        H_z_resized(i,i) = H_z_vec[i];
    //std::cout << "H_z_resized " << H_z_resized << std::endl;

    R_resized.conservativeResize(obs_size, obs_size);
    R_resized.setZero();
    for(int i =0; i < obs_size; ++i)
        R_resized(i,i) = R(0,0) /*+ i*0.1*/ ;
    //std::cout << "R_resized " << R_resized  << std::endl;

    S = H_x_resized * p_kk * H_x_resized.transpose().eval() + H_z_resized * R_resized * H_z_resized.transpose().eval();
    //std::cout << "S " << S << std::endl;
    K = p_kk * (H_x_resized.transpose().eval()) * (S.inverse());
    //std::cout << "K " << K << std::endl;
    x_kk = x_kk + K*V_resized;
    //std::cout << "x_kk " << x_kk << std::endl;
    p_kk = (I.setIdentity(state_size, state_size) - K*H_x_resized)*p_kk;
    //std::cout << "p_kk " << p_kk << std::endl;

    //    z_map_est = x_kk(0) - current_planes[i].data;
    //    V(0) = map[min_element.obs_id].data - z_map_est;
    //    S = H_x * p_kk * H_x.transpose().eval() + H_z * R * H_z.transpose().eval();
    //    K = p_kk * (H_x.transpose().eval()) * (S.inverse());
    //    x_kk = x_kk + K*V;
    //    p_kk = (I.setIdentity(state_size, state_size) - K*H_x)*p_kk;
    //    std::cout << "x_kk " << x_kk << std::endl;

    //adding the map elements
    if(!not_matched_planes.empty())
    {
        for (int i =0; i < not_matched_planes.size(); i++)
        {
            if (x_kk(0) - not_matched_planes[i] >= 0)
            {
                obs_vector new_map_element;
                new_map_element.obs_id = map.size() -1;
                new_map_element.data = x_kk(0) - not_matched_planes[i];
                map.push_back(new_map_element);
                std::cout << "added a new map element which is " << new_map_element.data << std::endl;
            }
            else
                std::cout << "rejecting an incorrectly detected plane " << std::endl;
        }
    }



    return x_kk(0);
}

